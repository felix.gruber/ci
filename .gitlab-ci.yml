---

stages:
  - service
  - base
  - core
  - module dependencies
  - modules
  - finalize
  - rollback


# ---------------------------------------------------------------------
#
# common variables for the various stages
#
# ---------------------------------------------------------------------

.service-variables:
  variables: &service-variables
    DUNECI_DOCKER_CACHE_SERVICE: 1
    CORES: 1

.base-variables:
  variables: &base-variables
    IMAGE: $CI_REGISTRY_IMAGE/$DISTRO:$VERSION
    SOURCE_DIRS: base-common $DISTRO-$VERSION
    DUNECI_DOCKER_CACHE_BASE: 1
    CORES: 1

.core-variables:
  variables: &core-variables
    IMAGE: $CI_REGISTRY_IMAGE/${CI_JOB_NAME}
    SOURCE_DIRS: dune-$VERSION
    BUILD_ARGS: --parallel
    DUNECI_DOCKER_CACHE_CORE: 1
    CORES: 4

.module-deps-variables:
  variables: &module-deps-variables
    IMAGE: $CI_REGISTRY_IMAGE/${CI_JOB_NAME}
    SOURCE_DIRS: dune-$MODULE-deps-$VERSION
    BUILD_ARGS: --parallel
    DUNECI_DOCKER_CACHE_MODULE_DEPS: 1
    CORES: 4

.module-variables:
  variables: &module-variables
    IMAGE: $CI_REGISTRY_IMAGE/${CI_JOB_NAME}
    SOURCE_DIRS: dune-$MODULE-$VERSION
    BUILD_ARGS: --parallel
    DUNECI_DOCKER_CACHE_MODULES: 1
    CORES: 4


# ---------------------------------------------------------------------
#
# Base template for all jobs that build a Docker image
#
# ---------------------------------------------------------------------


.base: &base
  before_script:
    - echo $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin=true $CI_REGISTRY
  script:
    - echo "Building $IMAGE..."
    - ./bin/duneci-build-docker-image $BUILD_ARGS $IMAGE $SOURCE_DIRS
  artifacts:
    paths:
      - output/images/
    expire_in: 1d
  tags:
    - "services:dind"
    - "dind:cores:4"
  only:
    refs:
      - branches@docker/ci


# ---------------------------------------------------------------------
#
# image templates for the various stages
#
# ---------------------------------------------------------------------


.service-image: &service-image
  <<: *base
  stage: service

.base-image: &base-image
  <<: *base
  stage: base

.core-image: &core-image
  <<: *base
  stage: core

.module-deps-image: &module-deps-image
  <<: *base
  stage: module dependencies

.module-image: &module-image
  <<: *base
  stage: modules


# ---------------------------------------------------------------------
#
# service images
#
# ---------------------------------------------------------------------


docker:
  <<: *service-image
  variables:
    <<: *service-variables
    IMAGE: $CI_REGISTRY_IMAGE/service/docker:latest
    SOURCE_DIRS: docker


# ---------------------------------------------------------------------
#
# base images
#
# ---------------------------------------------------------------------


debian:9:
  <<: *base-image
  variables:
    <<: *base-variables
    DISTRO: debian
    VERSION: 9

debian:10:
  <<: *base-image
  variables:
    <<: *base-variables
    DISTRO: debian
    VERSION: 10

ubuntu:16.04:
  <<: *base-image
  variables:
    <<: *base-variables
    DISTRO: ubuntu
    VERSION: "16.04"

ubuntu:18.04:
  <<: *base-image
  variables:
    <<: *base-variables
    DISTRO: ubuntu
    VERSION: "18.04"


# ---------------------------------------------------------------------
#
# core images
#
# ---------------------------------------------------------------------


dune:git-debian-9-gcc-6-14:
  <<: *core-image
  variables:
    <<: *core-variables
    VERSION: "git"
    BASE_IMAGE: "debian:9"
    TOOLCHAIN: "gcc-6-14"

dune:git-debian-10-gcc-8-17:
  <<: *core-image
  variables:
    <<: *core-variables
    VERSION: "git"
    BASE_IMAGE: "debian:10"
    TOOLCHAIN: "gcc-8-17"

dune:git-debian-10-gcc-8-noassert-17:
  <<: *core-image
  variables:
    <<: *core-variables
    VERSION: "git"
    BASE_IMAGE: "debian:10"
    TOOLCHAIN: "gcc-8-noassert-17"

dune:git-debian-10-gcc-7-14:
  <<: *core-image
  variables:
    <<: *core-variables
    VERSION: "git"
    BASE_IMAGE: "debian:10"
    TOOLCHAIN: "gcc-7-14"
    EXTRA_TAG: git

dune:git-debian-10-clang-6-libcpp-17:
  <<: *core-image
  variables:
    <<: *core-variables
    VERSION: "git"
    BASE_IMAGE: "debian:10"
    TOOLCHAIN: "clang-6-libcpp-17"

dune:git-ubuntu-18.04-clang-6-17:
  <<: *core-image
  variables:
    <<: *core-variables
    VERSION: "git"
    BASE_IMAGE: "ubuntu:18.04"
    TOOLCHAIN: "clang-6-17"

# there isn't a lot to configure for the 2.4 image, as it uses the
# distribution packages, which locks in both distribution and toolchain
dune:2.4-ubuntu-16.04-gcc-5-14:
  <<: *core-image
  variables:
    <<: *core-variables
    VERSION: "2.4"
    EXTRA_TAG: "2.4"

dune:2.5-debian-9-gcc-6-14:
  <<: *core-image
  variables:
    <<: *core-variables
    VERSION: "2.5"
    BASE_IMAGE: "debian:9"
    TOOLCHAIN: "gcc-6-14"
    EXTRA_TAG: "2.5"

dune:2.6-debian-9-gcc-6-14:
  <<: *core-image
  variables:
    <<: *core-variables
    VERSION: "2.6"
    BASE_IMAGE: "debian:9"
    TOOLCHAIN: "gcc-6-14"

dune:2.6-debian-10-gcc-7-14:
  <<: *core-image
  variables:
    <<: *core-variables
    VERSION: "2.6"
    BASE_IMAGE: "debian:10"
    TOOLCHAIN: "gcc-7-14"
    EXTRA_TAG: "2.6"

dune:2.6-debian-10-gcc-7-noassert-14:
  <<: *core-image
  variables:
    <<: *core-variables
    VERSION: "2.6"
    BASE_IMAGE: "debian:10"
    TOOLCHAIN: "gcc-7-noassert-14"

dune:2.6-debian-10-gcc-8-17:
  <<: *core-image
  variables:
    <<: *core-variables
    VERSION: "2.6"
    BASE_IMAGE: "debian:10"
    TOOLCHAIN: "gcc-8-17"

dune:2.6-debian-10-clang-6-libcpp-17:
  <<: *core-image
  variables:
    <<: *core-variables
    VERSION: "2.6"
    BASE_IMAGE: "debian:10"
    TOOLCHAIN: "clang-6-libcpp-17"

dune:2.6-ubuntu-18.04-clang-6-17:
  <<: *core-image
  variables:
    <<: *core-variables
    VERSION: "2.6"
    BASE_IMAGE: "ubuntu:18.04"
    TOOLCHAIN: "clang-6-17"


# ---------------------------------------------------------------------
#
# images with dependencies for building a module
#
# ---------------------------------------------------------------------


dune-pdelab-deps:2.6-debian-9-gcc-6-14:
  <<: *module-deps-image
  variables:
    <<: *module-deps-variables
    MODULE: pdelab
    VERSION: "2.6"
    BASE_IMAGE: "dune:2.6-debian-9-gcc-6-14"

dune-pdelab-deps:2.6-debian-10-gcc-7-noassert-14:
  <<: *module-deps-image
  variables:
    <<: *module-deps-variables
    MODULE: pdelab
    VERSION: "2.6"
    BASE_IMAGE: "dune:2.6-debian-10-gcc-7-noassert-14"

dune-pdelab-deps:2.6-debian-10-gcc-8-17:
  <<: *module-deps-image
  variables:
    <<: *module-deps-variables
    MODULE: pdelab
    VERSION: "2.6"
    BASE_IMAGE: "dune:2.6-debian-10-gcc-8-17"
    EXTRA_TAG: "2.6"

dune-pdelab-deps:2.6-debian-10-clang-6-libcpp-17:
  <<: *module-deps-image
  variables:
    <<: *module-deps-variables
    MODULE: pdelab
    VERSION: "2.6"
    BASE_IMAGE: "dune:2.6-debian-10-clang-6-libcpp-17"

dune-pdelab-deps:git-debian-10-gcc-7-14:
  <<: *module-deps-image
  variables:
    <<: *module-deps-variables
    MODULE: pdelab
    VERSION: git
    BASE_IMAGE: "dune:git-debian-10-gcc-7-14"
    EXTRA_TAG: "git"

dune-pdelab-deps:git-debian-10-gcc-8-noassert-17:
  <<: *module-deps-image
  variables:
    <<: *module-deps-variables
    MODULE: pdelab
    VERSION: git
    BASE_IMAGE: "dune:git-debian-10-gcc-8-noassert-17"

dune-pdelab-deps:git-debian-10-clang-6-libcpp-17:
  <<: *module-deps-image
  variables:
    <<: *module-deps-variables
    MODULE: pdelab
    VERSION: "git"
    BASE_IMAGE: "dune:git-debian-10-clang-6-libcpp-17"


# ---------------------------------------------------------------------
#
# module images
#
# ---------------------------------------------------------------------


dune-fufem:2.4:
  <<: *module-image
  variables:
    <<: *module-variables
    MODULE: fufem
    VERSION: "2.4"

dune-fufem:git:
  <<: *module-image
  variables:
    <<: *module-variables
    MODULE: fufem
    VERSION: "git"


dune-pdelab:2.6-debian-9-gcc-6-14:
  <<: *module-image
  variables:
    <<: *module-variables
    MODULE: pdelab
    VERSION: "2.6"
    BASE_IMAGE: "dune-pdelab-deps:2.6-debian-9-gcc-6-14"

dune-pdelab:2.6-debian-10-gcc-7-noassert-14:
  <<: *module-image
  variables:
    <<: *module-variables
    MODULE: pdelab
    VERSION: "2.6"
    BASE_IMAGE: "dune-pdelab-deps:2.6-debian-10-gcc-7-noassert-14"

dune-pdelab:2.6-debian-10-gcc-8-17:
  <<: *module-image
  variables:
    <<: *module-variables
    MODULE: pdelab
    VERSION: "2.6"
    BASE_IMAGE: "dune-pdelab-deps:2.6-debian-10-gcc-8-17"
    EXTRA_TAG: "2.6"

dune-pdelab:2.6-debian-10-clang-6-libcpp-17:
  <<: *module-image
  variables:
    <<: *module-variables
    MODULE: pdelab
    VERSION: "2.6"
    BASE_IMAGE: "dune-pdelab-deps:2.6-debian-10-clang-6-libcpp-17"

dune-pdelab:git-debian-10-gcc-7-14:
  <<: *module-image
  variables:
    <<: *module-variables
    MODULE: pdelab
    VERSION: git
    BASE_IMAGE: "dune-pdelab-deps:git-debian-10-gcc-7-14"
    EXTRA_TAG: "git"

dune-pdelab:git-debian-10-gcc-8-noassert-17:
  <<: *module-image
  variables:
    <<: *module-variables
    MODULE: pdelab
    VERSION: git
    BASE_IMAGE: "dune-pdelab-deps:git-debian-10-gcc-8-noassert-17"

dune-pdelab:git-debian-10-clang-6-libcpp-17:
  <<: *module-image
  variables:
    <<: *module-variables
    MODULE: pdelab
    VERSION: "git"
    BASE_IMAGE: "dune-pdelab-deps:git-debian-10-clang-6-libcpp-17"


# ---------------------------------------------------------------------
#
# deploy / cleanup
#
# ---------------------------------------------------------------------


cleanup-temp-images:
  stage: finalize
  image: $CI_REGISTRY_IMAGE/service/docker:latest
  variables:
    CORES: 1
  script:
    - ./bin/duneci-process-images cleanup
  tags:
    - "site:hd"
    - "serialized"
  only:
    refs:
      - branches@docker/ci
  except:
    refs:
      - master@docker/ci

deploy:
  environment:
    name: production
    url: https://gitlab.dune-project.org/docker/ci/container_registry
  stage: finalize
  image: $CI_REGISTRY_IMAGE/service/docker:latest
  variables:
    CORES: 1
  script:
    - ./bin/duneci-process-images deploy
  tags:
    - "site:hd"
    - "serialized"
  only:
    refs:
      - master@docker/ci

restore-images:
  stage: rollback
  when: on_failure
  image: $CI_REGISTRY_IMAGE/service/docker:latest
  variables:
    CORES: 1
  script:
    - ./bin/duneci-process-images rollback
  tags:
    - "site:hd"
    - "serialized"
  only:
    refs:
      - branches@docker/ci
